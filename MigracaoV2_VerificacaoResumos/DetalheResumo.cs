﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigracaoV2_VerificacaoResumos
{
    public class DetalheResumo
    {
        public string MsgStatus { get; set; }
        public int Qtd { get; set; }
        public int StatusEnvio { get; set; }
    }
    
    public class TotalResumo
    {
        public int TotalNeutro { get; set; }
        public int TotalEntregue { get; set; }
        public int TotalNaoEntregue { get; set; }

        public TotalResumo()
        {
            this.TotalEntregue = 0;
            this.TotalNaoEntregue = 0;
            this.TotalNeutro = 0;
        }
    }
}
