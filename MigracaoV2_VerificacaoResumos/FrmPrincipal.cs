﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Banco;
using System.Threading;
using static MigracaoV2_VerificacaoResumos.StatusHandler;
using MC.Infra.Servicos.Azure;
using System.Configuration;

namespace MigracaoV2_VerificacaoResumos
{
    public partial class FrmPrincipal : Form
    {

        event OnEventoProgressoHandler OnEventoProgresso;

        public FrmPrincipal()
        {
            InitializeComponent();
            this.OnEventoProgresso += (idx, txt, img, s, tb) => { AtualizarItemListView(idx, txt, img, s, tb); };

            CarregaComboClientes();
        }

        private void CarregaComboClientes()
        {
            var bd = new AcessoDados();
            var dt = bd.ExecuteDataTable("select 0 as Id, '-Todos' as Nome union select c.id, c.nome from sqlazure.messagecenter_br.dbo.clientes c where c.ativo=1 order by nome");
            cbClientes.DataSource = dt;
            cbClientes.DisplayMember = "nome";
            cbClientes.ValueMember = "id";
            
        }

        private void AtualizarItemListView(int index, string texto, int imageIndex, bool sucesso, TabelaResultados tabela)
        {
            listView1.BeginInvoke((MethodInvoker)delegate
            {
                if (texto != "")
                {
                    listView1.Items[index].ToolTipText += Environment.NewLine + texto;
                }
                listView1.Items[index].ImageIndex = imageIndex;
            });

            //if (sucesso)
            //{
            //    listView1.BeginInvoke((MethodInvoker)delegate
            //    {
            //        listView1.Items[index].Tag = "ok";
            //    });
            //}
            //else
            //{
            //    listView1.BeginInvoke((MethodInvoker)delegate
            //    {
            //        listView1.Items[index].Tag = "erro";
            //    });
            //}

            if (tabela != null)
            {
                tabela.sucesso = sucesso;

                listView1.BeginInvoke((MethodInvoker)delegate
                {
                    listView1.Items[index].Tag = tabela;
                });
            }

            Application.DoEvents();
            //}
            //else
            //{
            //    listView1.Items[index].ImageIndex = imageIndex;
            //    listView1.Refresh();
            //}

            //    )
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            lbStatus.Text = "Consultando campanhas...";
            //buscar em Campanhas (v1) por período de aprovação
            var campanhas = ConsultaCampanhas(dt_ini.Value.Date, dt_fim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59));
            lbStatus.Text = "Consulta finalizada. " + campanhas.Count + " registros encontrados.";

            int ix = 0;
            listView1.ShowItemToolTips = true;
            foreach (var item in campanhas)
            {
                var li = new ListViewItem(item.Id.ToString()+" - "+item.Nome);
                li.ToolTipText = "LoteId: " + item.LoteId +  Environment.NewLine +
                    "Data autorização: " + item.DataAutorizacao + Environment.NewLine +
                    "CampanhaId: " +item.Id+" - "+item.Nome+Environment.NewLine+
                    "Cliente: "+item.ClienteNome ;
                li.ImageIndex = -1;
                listView1.Items.Add(li);
                item.index = ix;
                ix++;
            }

            IniciaProcessamento(campanhas);
            //se encontrar... procura na Tables
            //se encontrar compara
            //......quantidade de linhas
            //......total com total
            //......bater qtd por status
            //se não encontrar gera inconsistencia
        }

        private void IniciaProcessamento(List<Campanha> campanhas)
        {


            Parallel.For(0, campanhas.Count, new ParallelOptions { MaxDegreeOfParallelism = Convert.ToInt32(txtThreads.Text) }, i =>
            {
                try
                {
                    var campanhaId = campanhas[i].Id;

                    VerificaCampanha(campanhas[i], Convert.ToInt32(campanhaId), i);
                }
                catch (Exception ex)
                {
                    this.OnEventoProgresso(i, ex.Message.ToString(), 2, false, null);//terminou erro
                }
            });
            MessageBox.Show("Concluído");
        }

        private void VerificaCampanha(Campanha Campanha, int campanhaId, int index)
        {
            this.OnEventoProgresso(index, "", 0, true, null);//em processamento

            //consulta resumo SQL
            var resumoSQL = new List<DetalheResumo>();
            var bd = new AcessoDados();

            var nometabela = "mensagens_"+Campanha.LoteId.ToString();

            var script = " select isnull(m.statusenvio, 6) as StatusEnvio, isnull(m.MsgStatus, 'Aguardando confirmação da entrega') as MsgStatus, count(*) QtdStatus"+
               " from "+nometabela+" m with(nolock) "+
               " inner join(select m1.loteid, m1.idauxiliar, m1.statusenvio, m1.autoid, m1.datastatus, m1.msgstatus, m1.DataTransmissao, "+
               "                  ROW_NUMBER() OVER(PARTITION BY m1.loteid, isnull(m1.Idauxiliar, m1.IdEnvioExterno) " +
               "                  ORDER BY m1.statusenvio, m1.datastatus Desc) AS Row " +
               "           from "+nometabela+" m1 with(nolock)) as Q on Q.autoid = m.autoid " +
               " where Q.Row = 1 " +
               " and m.amostra = 0 " +
               " group by m.statusenvio, m.msgstatus";



            using (var dt = bd.ExecuteDataTable(script))
            {
                foreach (DataRow registro in dt.Rows)
                {
                    var r = new DetalheResumo()
                    {
                        MsgStatus = Convert.ToString(registro["MsgStatus"]),
                        Qtd = Convert.ToInt32(registro["QtdStatus"]),
                        StatusEnvio = Convert.ToInt32(registro["StatusEnvio"])
                    };
                    if (r.MsgStatus == "Aguardando confirmação da entrega")
                        r.StatusEnvio = 6;
                    if (r.MsgStatus.StartsWith("Download"))
                        r.StatusEnvio = 2;
                    resumoSQL.Add(r);
                }
            };

            //consulta resumo tables
            var config = new AppSettingsReader();
            string str = config.GetValue("str-azure-storage", typeof(string)).ToString();
            var servTables = new ServicoAzureTables(str);
            List<CampanhasDetalhesNoSQL> detalhesTb = servTables.ConsultarDocumentosDeUmaParticao<CampanhasDetalhesNoSQL>(campanhaId.ToString(), "CampanhasDetalhes").Where(f => f.Grupo == "Padrao" && f.Status != 0).ToList();

            foreach (var item in detalhesTb)
            {
                if (item.Informacao.StartsWith("Download"))
                    item.Status = 2;
            }

            var achouErro = false;
            string logErro = "";

            TotalResumo totalTmp = new TotalResumo();
            TotalResumo totalTables = new TotalResumo();



            foreach (var regSQL in resumoSQL)
            {
                switch (regSQL.StatusEnvio)
                {
                    case 2:
                        {
                            totalTmp.TotalEntregue += regSQL.Qtd;
                            break;
                        }
                    case 3:
                        {
                            totalTmp.TotalNaoEntregue += regSQL.Qtd;
                            break;
                        }
                    case 6:
                        {
                            totalTmp.TotalNeutro += regSQL.Qtd;
                            break;
                        }
                    default:
                        break;
                }
            }

            foreach (var regTB in detalhesTb)
            {
                switch (regTB.Status)
                {
                    case 2:
                        {
                            totalTables.TotalEntregue += Convert.ToInt32(regTB.Informacao);
                            break;
                        }
                    case 3:
                        {
                            totalTables.TotalNaoEntregue += Convert.ToInt32(regTB.Informacao);
                            break;
                        }
                    case 6:
                        {
                            if (regTB.Grupo != "Higienizados")
                            {
                                totalTables.TotalNeutro += Convert.ToInt32(regTB.Informacao);
                            }
                            else
                            {

                            };
                            break;
                        }
                    default: //default é neutro
                        {
                            totalTables.TotalNeutro += Convert.ToInt32(regTB.Informacao);
                            break;
                        }
                }
            }

         
            //compara TOTAL GERAL
            int _totalTables = (totalTables.TotalEntregue + totalTables.TotalNaoEntregue + totalTables.TotalNeutro);
            int _totalTemporaria = (totalTmp.TotalEntregue + totalTmp.TotalNaoEntregue + totalTmp.TotalNeutro);


            //COMPARATIVO DE "NÃO ENTREGAS"
            int percMinNaoEntrega = Convert.ToInt32(txtPercNaoEntrega.Text);
            if (((totalTmp.TotalNaoEntregue *1.0 / _totalTemporaria * 100.00)>=percMinNaoEntrega) && totalTmp.TotalNaoEntregue > Convert.ToInt32(txtQtdeMinNaoEntrega.Text))
            {
                //mais de x% de não entrega
                achouErro = true;
                logErro += Environment.NewLine + "Total de não entregue alto: " + totalTmp.TotalNaoEntregue + " => " + (totalTmp.TotalNaoEntregue *1.0/ _totalTemporaria * 100.00) + "%";
            }

            //compara total SQL x TABLES
            int diferenca = Math.Abs(_totalTables - _totalTemporaria);
            if (_totalTables > 0)
            {
                if (((diferenca / _totalTables) * 100.00) > 10)
                {
                    achouErro = true;
                    logErro += Environment.NewLine + "Total geral diferente: TMP=" + (totalTmp.TotalEntregue + totalTmp.TotalNaoEntregue + totalTmp.TotalNeutro) + " TABLES=" + (totalTables.TotalEntregue + totalTables.TotalNaoEntregue + totalTables.TotalNeutro);
                }
            }
            
            //compara aguardando confirmação de entrega
            int aguardandoConfiEntrega = 0;
            var LinhaAguardandoConf = resumoSQL.FirstOrDefault(f => f.MsgStatus == "Aguardando confirmação da entrega");
            if (LinhaAguardandoConf != null)
            {
                var LinhaAguardandoConfTables = detalhesTb.FirstOrDefault(f => f.Item == "Aguardando confirmação da entrega");


                int percMinAguardandoConfirmacaoEntrega = Convert.ToInt32(txtPercAguardConfirmacaoEntrega.Text);
                if (((LinhaAguardandoConf.Qtd * 1.0 / _totalTemporaria * 100.00) >= percMinAguardandoConfirmacaoEntrega) 
                    && LinhaAguardandoConf.Qtd > Convert.ToInt32(txtQtdeMinConfirmacaoEntrega.Text))
                {
                    //mais de x% de não entrega
                    achouErro = true;
                    logErro += Environment.NewLine + "Total de 'Aguardando confirmação' alto: " + LinhaAguardandoConf.Qtd +
                        " => " + (LinhaAguardandoConf.Qtd * 1.0 / _totalTemporaria * 100.00) + "%";
                }

                //if (LinhaAguardandoConfTables == null)
                //{
                //achouErro = true;
                // logErro += Environment.NewLine + "Tem registro Aguardando conf. entrega na TMP (" + LinhaAguardandoConf.Qtd + ") e não tem na tables";
                //}
                //else
                //{
                //if (LinhaAguardandoConf.Qtd > Convert.ToInt32(LinhaAguardandoConfTables.Informacao))
                //{
                // achouErro = true;
                // logErro += Environment.NewLine + "Quantidade Aguardando conf. entrega na TMP (" + LinhaAguardandoConf.Qtd + ") é maior que na tables (" +
                //      LinhaAguardandoConfTables.Informacao + ")";
                //}
                //else
                // {
                //verifica o % de aguardando confirmação de entrega em relação ao total de mensagens
                //double perc = LinhaAguardandoConf.Qtd * 1.0 / (totalTmp.TotalEntregue + totalTmp.TotalNaoEntregue + totalTmp.TotalNeutro);
                //if (perc * 100.00 > Convert.ToInt32(txtPercTemporarios.Text) && LinhaAguardandoConf.Qtd > Convert.ToInt32(txtQtdMinTemporarios.Text))
                //{
                //    achouErro = true;
                //    logErro += Environment.NewLine + "Quantidade aguardando conf. entrega maior que 10% => " + perc.ToString("N");
                //}
                //}
                //}
            }

            //compara o ERRO TEMPORARIO
            var LinhaErroTemporario = resumoSQL.FirstOrDefault(f => f.MsgStatus == "Erro temporário");
            if (LinhaErroTemporario != null)
            {
                double perc = LinhaErroTemporario.Qtd * 1.0 / (totalTmp.TotalEntregue + totalTmp.TotalNaoEntregue + totalTmp.TotalNeutro);
                if (perc * 100.00 > Convert.ToInt32(txtPercTemporarios.Text) && LinhaErroTemporario.Qtd > Convert.ToInt32(txtQtdMinTemporarios.Text))
                {
                    achouErro = true;
                    logErro += Environment.NewLine + "Quantidade Erro Temporário maior que "+txtPercTemporarios.Text+"% => " + (perc*100.00).ToString("N");
                }
            }

            //compara o entregue com sucesso
            //var difEntregue=(totalTmp.TotalEntregue - totalTables.TotalEntregue);
            //var percDiferencaEntregue = difEntregue * 1.0 / totalTables.TotalEntregue;
            //if (percDiferencaEntregue > 0.1 & totalTmp.TotalEntregue < totalTables.TotalEntregue && diferenca > 4)
            //{
                //achouErro = true;
                //logErro += Environment.NewLine + "Total Entregue menor: TMP=" + (totalTmp.TotalEntregue) + " TABLES=" + (totalTables.TotalEntregue) + " => "+percDiferencaEntregue.ToString()+"%";
            //}


            //if (achouErro)
            //{
            logErro += Environment.NewLine + "---" + Environment.NewLine + "Totalizadores:" + Environment.NewLine +
                "Entregues TMP: " + totalTmp.TotalEntregue + " => TABLES: " + totalTables.TotalEntregue + Environment.NewLine +
                "Não Entregues TMP: " + totalTmp.TotalNaoEntregue + " => TABLES: " + totalTables.TotalNaoEntregue + Environment.NewLine +
                "Neutro TMP: " + totalTmp.TotalNeutro + " => TABLES: " + totalTables.TotalNeutro;

            var tbResult = new TabelaResultados();

            //detalha o erro
            logErro += Environment.NewLine + "---" + Environment.NewLine + "Linhas da TABLES:";
            foreach (var itemtb in detalhesTb.OrderBy(f => f.Item))
            {
                logErro += Environment.NewLine + itemtb.Item + " = " + itemtb.Informacao;
                tbResult.Add(new LinhaTabelaResultados()
                {
                    Grupo = itemtb.Status.ToString(),
                    Status = itemtb.Item,
                    QtdTables = itemtb.Informacao,
                    Diferenca = itemtb.Informacao
                });
            }
            logErro += Environment.NewLine + "---" + Environment.NewLine + "Linhas da TEMPORARIA:";
            foreach (var itemTmp in resumoSQL)
            {
                logErro += Environment.NewLine + itemTmp.MsgStatus + " = " + itemTmp.Qtd;

                //procura um correspondente na tables.. se encontrar atualiza, senão insere
                var rowTbResultado = tbResult.FirstOrDefault(f => f.Status == itemTmp.MsgStatus
                && f.Grupo == itemTmp.StatusEnvio.ToString());
                if (rowTbResultado != null)
                {
                    rowTbResultado.Status = itemTmp.MsgStatus;
                    rowTbResultado.QtdTmp = itemTmp.Qtd.ToString();
                    rowTbResultado.Diferenca = (Convert.ToInt32(rowTbResultado.QtdTables) - Convert.ToInt32(rowTbResultado.QtdTmp)).ToString();
                }
                else
                {
                    tbResult.Add(new LinhaTabelaResultados()
                    {
                        Grupo = itemTmp.StatusEnvio.ToString(),
                        Status = itemTmp.MsgStatus,
                        QtdTmp = itemTmp.Qtd.ToString(),
                        Diferenca = (0 - Convert.ToInt32(itemTmp.Qtd)).ToString()
                    });
                }
            }

    
           

            if (!achouErro)
            {
                this.OnEventoProgresso(index, logErro, 1, true, tbResult);//terminou sucesso
            }
            else
            {
                this.OnEventoProgresso(index, logErro, 2, false, tbResult);//terminou erro
            }
        }

        private List<Campanha> ConsultaCampanhas(DateTime value1, DateTime value2)
        {
            var Lista = new List<Campanha>();
            string sql = "";
            if (rbPeriodo.Checked)
            {
                sql = "select c.nome, cli.nome as clientenome, l.id loteid, l.dataautorizacaoenvio, c.id campanhaid" +
                " from sqlazure.messagecenter_br.dbo.loteprocessamento l " +
                " inner join sqlazure.messagecenter_br.dbo.clientes cli on cli.id=l.clienteid " +
                " inner join sqlazure.messagecenter_br.dbo.campanhas c on c.loteid = l.id " +
                " where l.dataautorizacaoenvio between @d1 and @d2 and (agendamento is null or agendamento<=getdate())";

                if (Convert.ToInt32(cbClientes.SelectedValue) > 0)
                {
                    sql += " and c.clienteId=" + Convert.ToInt32(cbClientes.SelectedValue).ToString();
                }
            }
            else
            {
                sql = "select c.nome, cli.nome as clientenome, l.id loteid, l.dataautorizacaoenvio, c.id campanhaid" +
                " from sqlazure.messagecenter_br.dbo.loteprocessamento l " +
                " inner join sqlazure.messagecenter_br.dbo.clientes cli on cli.id=l.clienteid " +
                " inner join sqlazure.messagecenter_br.dbo.campanhas c on c.loteid = l.id " +
                " where c.id=" + txtCampanhaId.Text;
            }
            var bd = new AcessoDados();
            using (var dt = bd.ExecuteDataTable(sql, new Parametro("@d1", value1), new Parametro("@d2", value2)))
            {
                foreach (DataRow registro in dt.Rows)
                {
                    Lista.Add(new Campanha()
                    {
                        Id = Convert.ToInt32(registro["campanhaid"]),
                        LoteId = Convert.ToInt32(registro["LoteId"]),
                        DataAutorizacao = Convert.ToString(registro["dataautorizacaoenvio"]),
                        Nome = registro["nome"].ToString(),
                        ClienteNome = registro["clientenome"].ToString()
                    });
                }
            }
            return Lista;
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            //MessageBox.Show("CampanhaId = " + listView1.SelectedItems[0].Text + Environment.NewLine +
            //    listView1.SelectedItems[0].ToolTipText);
            var f = new FrmVerResultados();
            f.SetTabelaResultados((TabelaResultados)listView1.SelectedItems[0].Tag);
            f.SetResultadoTexto(listView1.SelectedItems[0].ToolTipText);
            f.ShowDialog();
        }

        private void btnApagaOK_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Tag != null &&
                    item.Tag.GetType() == typeof(TabelaResultados) &&
                    ((TabelaResultados)item.Tag).sucesso)
                    listView1.Items.Remove(item);
            }
            MessageBox.Show("Concluído");
        }
    }
}
