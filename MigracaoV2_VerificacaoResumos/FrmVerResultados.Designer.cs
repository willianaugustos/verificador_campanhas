﻿namespace MigracaoV2_VerificacaoResumos
{
    partial class FrmVerResultados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gvResultados = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tbResultadoTexto = new System.Windows.Forms.TextBox();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdTmp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdTables = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Diferenca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvResultados)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(965, 554);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gvResultados);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(957, 528);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tabela";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gvResultados
            // 
            this.gvResultados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Status,
            this.QtdTmp,
            this.QtdTables,
            this.Diferenca});
            this.gvResultados.Location = new System.Drawing.Point(3, 6);
            this.gvResultados.Name = "gvResultados";
            this.gvResultados.Size = new System.Drawing.Size(948, 516);
            this.gvResultados.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tbResultadoTexto);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(957, 528);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Texto";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tbResultadoTexto
            // 
            this.tbResultadoTexto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResultadoTexto.Location = new System.Drawing.Point(6, 6);
            this.tbResultadoTexto.Multiline = true;
            this.tbResultadoTexto.Name = "tbResultadoTexto";
            this.tbResultadoTexto.ReadOnly = true;
            this.tbResultadoTexto.Size = new System.Drawing.Size(945, 516);
            this.tbResultadoTexto.TabIndex = 0;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 300;
            // 
            // QtdTmp
            // 
            this.QtdTmp.DataPropertyName = "QtdTmp";
            this.QtdTmp.HeaderText = "QtdTmp";
            this.QtdTmp.Name = "QtdTmp";
            // 
            // QtdTables
            // 
            this.QtdTables.DataPropertyName = "QtdTables";
            this.QtdTables.HeaderText = "QtdTables";
            this.QtdTables.Name = "QtdTables";
            // 
            // Diferenca
            // 
            this.Diferenca.DataPropertyName = "Diferenca";
            this.Diferenca.HeaderText = "Diferenca";
            this.Diferenca.Name = "Diferenca";
            // 
            // FrmVerResultados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 558);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmVerResultados";
            this.Text = "FrmVerResultados";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvResultados)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView gvResultados;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tbResultadoTexto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdTmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdTables;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diferenca;
    }
}