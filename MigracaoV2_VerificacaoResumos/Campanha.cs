﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigracaoV2_VerificacaoResumos
{
    public class Campanha
    {
        public int Id { get; set; }
        public int LoteId { get; set; }
        public int index { get; set; }
        public string DataAutorizacao { get; internal set; }
        public string Nome { get; internal set; }
        public string ClienteNome { get; internal set; }
    }
}
