﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigracaoV2_VerificacaoResumos
{
    public class TabelaResultados : List<LinhaTabelaResultados>
    {
        public bool sucesso { get; set; }
    }

    public class LinhaTabelaResultados
    {
        public string Grupo { get; set; }
        public string Status { get; set; }
        public string QtdTmp { get; set; }
        public string QtdTables { get; set; }
        public string Diferenca { get; set; }
    }
}
