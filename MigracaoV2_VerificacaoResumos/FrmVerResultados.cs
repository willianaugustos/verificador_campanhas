﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MigracaoV2_VerificacaoResumos
{
    public partial class FrmVerResultados : Form
    {
        public FrmVerResultados()
        {
            InitializeComponent();
        }

        public void SetTabelaResultados(TabelaResultados tabela)
        {
            if (tabela != null)
            {
                var dsTB = new TabelaResultados();

                int totalGeralTmp = 0;
                int totalGeralTables = 0;

                int totTmp = 0;
                int totTb = 0;
                dsTB.Add(new LinhaTabelaResultados()
                {
                    Status = "** ENTREGUES **"
                });
                foreach (var item in tabela.Where(f => f.Grupo == "2").ToList())
                {
                    dsTB.Add(item);
                    totTmp += Convert.ToInt32(item.QtdTmp);
                    totTb += Convert.ToInt32(item.QtdTables);
                }
                totalGeralTables += totTb;
                totalGeralTmp += totTmp;
                dsTB.Add(new LinhaTabelaResultados() { Status = "Total =>", QtdTables = totTb.ToString(), QtdTmp = totTmp.ToString() });//linha em branco

                dsTB.Add(new LinhaTabelaResultados());//linha em branco
                dsTB.Add(new LinhaTabelaResultados()
                {
                    Status = "** NÃO ENTREGUES **"
                });

                totTmp = 0;
                totTb = 0;
                foreach (var item in tabela.Where(f => f.Grupo == "3").ToList())
                {
                    dsTB.Add(item);
                    totTmp += Convert.ToInt32(item.QtdTmp);
                    totTb += Convert.ToInt32(item.QtdTables);
                }
                totalGeralTables += totTb;
                totalGeralTmp += totTmp;
                dsTB.Add(new LinhaTabelaResultados() { Status = "Total =>", QtdTables = totTb.ToString(), QtdTmp = totTmp.ToString() });//linha em branco

                dsTB.Add(new LinhaTabelaResultados());//linha em branco
                dsTB.Add(new LinhaTabelaResultados()
                {
                    Status = "** NEUTROS **"
                });
                totTmp = 0;
                totTb = 0;
                foreach (var item in tabela.Where(f => f.Grupo == "6").ToList())
                {
                    dsTB.Add(item);
                    totTmp += Convert.ToInt32(item.QtdTmp);
                    totTb += Convert.ToInt32(item.QtdTables);
                }
                totalGeralTables += totTb;
                totalGeralTmp += totTmp;
                dsTB.Add(new LinhaTabelaResultados() { Status = "Total =>", QtdTables = totTb.ToString(), QtdTmp = totTmp.ToString() });//linha em branco


                dsTB.Add(new LinhaTabelaResultados() { Status = "Total Geral =>", QtdTables = totalGeralTables.ToString(), QtdTmp = totalGeralTmp.ToString() });

                this.gvResultados.AutoGenerateColumns = false;
                this.gvResultados.DataSource = dsTB;
                this.gvResultados.Update();
                }
        }

        public void SetResultadoTexto(string texto)
        {
            this.tbResultadoTexto.Text = texto;
        }
    }
}
