﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MC.Infra.Servicos.Azure
{
    public class ServicoAzureTables
    {
        private string _stringDeConexao;

        public ServicoAzureTables(string stringDeConexao)
        {
            this._stringDeConexao = stringDeConexao;
        }


        #region Métodos genéricos

        public bool isEntidadeExistente<T>(string tableName, string ODataQuery)
            where T : TableEntity, new()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            string filter = ODataQuery;
            TableQuery<T> query = new TableQuery<T>().Where(filter);
            var entResult = table.ExecuteQuery(query).Select(ent => (T)ent).FirstOrDefault();

            if (entResult != null)
                return true;
            else
                return false;
        }

        public bool InserirDocumentoSeNaoExistir<T>(string tableName, string chaveParticao, string chaveRegistro, TableEntity propriedades)
             where T : TableEntity
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            table.CreateIfNotExists();
            propriedades.Timestamp = DateTime.Now;

            TableOperation retrieveOperation = TableOperation.Retrieve<T>(chaveParticao, chaveRegistro);

            TableResult retrievedResult = table.Execute(retrieveOperation);

            if (retrievedResult.Result == null)
            {
                TableOperation insertOperation = TableOperation.Insert(propriedades);
                table.Execute(insertOperation);
                return true;
            }
            else
                return false;
        }

        public T ConsultarDocumentoPorChaves<T>(string tableName, string partitionKey, string rowKey)
            where T : TableEntity
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            TableResult retrievedResult = table.Execute(retrieveOperation);

            return (T)retrievedResult.Result;
        }


        public T ConsultarDocumento<T>(string tableName, string ODataQuery)
            where T : TableEntity, new()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            string filter = ODataQuery;
            TableQuery<T> query = new TableQuery<T>().Where(filter);
            var result = table.ExecuteQuery(query).Select(ent => (T)ent).FirstOrDefault();

            return result;
        }


        #region Inserir ou Atualizar entidade única

        public void InserirEntidade<T>(string tableName, TableEntity entity)
            where T : TableEntity
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            TableOperation insertOperation = TableOperation.Insert(entity);
            table.Execute(insertOperation);
        }

        public void InserirOuAtualizarEntidade<T>(string tableName, TableEntity entity)
            where T : TableEntity
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(entity);
            table.Execute(insertOrReplaceOperation);
        }

        public void InserirOuAtualizarEntidade(string tableName, ITableEntity entity)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(entity);
            table.Execute(insertOrReplaceOperation);
        }

        /// <summary>
        /// Método genérico para inserir ou atualizar entidades em uma Table do Azure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <param name="entidade"></param>
        public void InserirOuAtualizarEntidade<T>(string tableName, string partitionKey, string rowKey, TableEntity entidade)
            where T : TableEntity
        {
            entidade.PartitionKey = partitionKey;
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(entidade);
            table.Execute(insertOrReplaceOperation);
        }

        #endregion


        #region Inserir ou Atualizar lista de entidades

        /// <summary>
        /// IMPORTANTE! Usar true para "useBatchOperation" APENAS se a PartitionKey for a MESMA para todas as entidades da lista a ser inserida
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="lstEntidades"></param>
        /// <param name="autogenerateRowKey"></param>
        /// <param name="useBatchOperation">Usar true APENAS se a PartitionKey for a MESMA para todas as inserções</param>
        public void InserirOuAtualizarListaDeEntidades<T>(string tableName, string partitionKey, List<TableEntity> lstEntidades, bool autogenerateRowKey, bool useBatchOperation)
            where T : TableEntity
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            int processosSucesso = 0;
            int processosErro = 0;
            var listaErros = new List<string>();
            if (useBatchOperation) //IMPORTANTE!!! SÓ UTILIZAR BATCH OPERATION SE A TODAS AS PARTITION KEYS DA LISTA FOREM A MESMA!!!
            {
                TableBatchOperation batchOperation = new TableBatchOperation();

                //while (lstEntidades.Count != 0)
                //{
                    //var listaTemp = lstEntidades.Take(100).ToList(); //alterar para 100 (máx permitido) após testes
                    foreach (var entidade in lstEntidades)
                    {
                        if (string.IsNullOrWhiteSpace(entidade.PartitionKey))
                        {
                            entidade.PartitionKey = partitionKey;
                        }

                        if (autogenerateRowKey)
                        {
                            entidade.RowKey = Guid.NewGuid().ToString();
                        }
                        processosSucesso++;
                        batchOperation.Insert(entidade);
                    }
                    try
                    {
                        table.ExecuteBatch(batchOperation);
                        batchOperation.Clear();
                    }
                    catch (Exception ex)
                    {
                       
                        listaErros.Add("Erro principal => " + ex.Message);
                        foreach (var item in lstEntidades)
                        {
                            try
                            {
                                TableOperation insertOrReplaceOperation;
                                insertOrReplaceOperation = TableOperation.InsertOrReplace(item);
                                table.Execute(insertOrReplaceOperation);
                            }
                            catch(Exception ex2)
                            {
                                processosSucesso--;
                                processosErro++;
                                listaErros.Add("Erro secundário => " + ex2.Message);
                                listaErros.Add("Objeto => " + item.ToString());
                            }
                        }
                    }
                    
                    //lstEntidades.RemoveAll(item => lstEntidades.Contains(item));
                //}
                if (processosErro > 0)
                {
                    var ex = new Exception();
                    listaErros.Add("Processo finalizado com " + processosErro + " erro(s)");
                    listaErros.Add("Quantidade de processos com sucesso : " + processosSucesso);
                    ex.Data["ListaErros"] = listaErros;

                    throw ex;
                }
            }
            else
            {
                TableOperation insertOrReplaceOperation;

                foreach (var entidade in lstEntidades)
                {
                    if (string.IsNullOrWhiteSpace(entidade.PartitionKey))
                    {
                        entidade.PartitionKey = partitionKey;
                    }

                    if (autogenerateRowKey)
                    {
                        entidade.RowKey = Guid.NewGuid().ToString();
                    }

                    insertOrReplaceOperation = TableOperation.InsertOrReplace(entidade);
                    table.Execute(insertOrReplaceOperation);
                }
            }
            
        }

        public void InserirOuAtualizarListaDeEntidades(string tableName, List<ITableEntity> lstEntidades)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            foreach (var entidade in lstEntidades)
            {
                TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(entidade);
                table.Execute(insertOrReplaceOperation);
            }
        }

        #endregion


        public List<T> ConsultarDocumentosDeUmaParticao<T>(string partitionKey, string tableName)
            where T : TableEntity, new()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            string filtro = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);

            TableQuery<T> query = new TableQuery<T>().Where(filtro);

            var results = table.ExecuteQuery<T>(query).Select(ent => (T)ent).ToList();
            return results;
        }

        public void ExcluirEntidade<T>(string tableName, string partitionKey, string rowKey)
            where T : TableEntity
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            TableResult retrievedResult = table.Execute(retrieveOperation);
            T deleteEntity = (T)retrievedResult.Result;

            if (deleteEntity != null)
            {
                TableOperation deleteOperation = TableOperation.Delete(deleteEntity);
                table.Execute(deleteOperation);
            }
        }

        /// <summary>
        /// Método Genérico para retornar uma lista de entidades
        /// </summary>
        /// <typeparam name="T">Tipo que herde de TableEntity</typeparam>
        /// <param name="tableName">Nome da Azure Table</param>
        /// <param name="ODataQuery">Query manual no formato OData</param>
        /// <returns></returns>
        public List<T> ObterListaDeEntidades<T>(string tableName, string ODataQuery)
            where T : TableEntity, new()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            string filter = ODataQuery;
            TableQuery<T> query = new TableQuery<T>().Where(filter);
            var results = table.ExecuteQuery(query).Select(ent => (T)ent).ToList();

            return results;
        }

        public List<DynamicTableEntity> ObterListaDeEntidades(string tableName, string ODataQuery)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);
            string filter = ODataQuery;
            TableQuery query = new TableQuery().Where(filter);
            var results = table.ExecuteQuery(query).Select(ent => (DynamicTableEntity)ent).ToList();

            return results;
        }

        public void ExcluirEntidadesPorParticao<TableEntity>(string TableName, string PartitionKey)
        {
            if (string.IsNullOrEmpty(PartitionKey))
                throw new ArgumentNullException();

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this._stringDeConexao);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(TableName);

            var batchOperation = new TableBatchOperation();

            // We need to pass at least one property to project or else 
            // all properties will be fetch in the operation
            var projectionQuery = new TableQuery<DynamicTableEntity>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey",
                    QueryComparisons.Equal, PartitionKey))
                .Select(new string[] { "RowKey" });

            int cont = 0;
            foreach (var e in table.ExecuteQuery(projectionQuery))
            {
                cont++;
                batchOperation.Delete(e);
                if (cont > 99)
                {
                    table.ExecuteBatch(batchOperation);
                    cont = 0;
                    batchOperation = new TableBatchOperation();
                }
            }

            if (cont > 0)
            {
                table.ExecuteBatch(batchOperation);
            }
        }

        #endregion

        


    }

    public class TestEntity : TableEntity
    {
        // public string AnoNascto { get; set; } // PK
        // public string CPF { get; set; } // RK
        public int Idade { get; set; }
        public string Nome { get; set; }
    }
}
