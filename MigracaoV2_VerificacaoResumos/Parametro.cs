﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Banco
{   
    public class Parametro
    {
        public string Nome { get; set; }
        public object Valor { get; set; }

        public Parametro(string nome, object valor)
        {
            this.Nome = nome;
            this.Valor = valor;
        }
    }

    /// <summary>
    /// "object Valor" deve ser um DataTable identico (colunas) ao "Table Type" do Sql Server
    /// Em "string SqlTypeName" informar o nome do Tabel Type, ex: dbo.ListaTipoInt
    /// </summary>
    public class ParametroDbType : Parametro
    {
        public string SqlTypeName { get; set; }

        public ParametroDbType(string nome, object valor, string SqlTypeName) : base(nome, valor)
        {
            base.Nome = nome;
            base.Valor = valor;
            this.SqlTypeName = SqlTypeName;
        }
    }
}
