﻿namespace MigracaoV2_VerificacaoResumos
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.label1 = new System.Windows.Forms.Label();
            this.dt_ini = new System.Windows.Forms.DateTimePicker();
            this.dt_fim = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_ok = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnApagaOK = new System.Windows.Forms.Button();
            this.txtPercNaoEntrega = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbClientes = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtThreads = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPercTemporarios = new System.Windows.Forms.TextBox();
            this.txtQtdMinTemporarios = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCampanhaId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.rbPeriodo = new System.Windows.Forms.RadioButton();
            this.rbCampanhaId = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtQtdeMinNaoEntrega = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtQtdeMinConfirmacaoEntrega = new System.Windows.Forms.TextBox();
            this.txtPercAguardConfirmacaoEntrega = new System.Windows.Forms.TextBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Período (data aprovação do lote)";
            // 
            // dt_ini
            // 
            this.dt_ini.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_ini.Location = new System.Drawing.Point(40, 44);
            this.dt_ini.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dt_ini.Name = "dt_ini";
            this.dt_ini.Size = new System.Drawing.Size(115, 22);
            this.dt_ini.TabIndex = 1;
            // 
            // dt_fim
            // 
            this.dt_fim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_fim.Location = new System.Drawing.Point(196, 46);
            this.dt_fim.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dt_fim.Name = "dt_fim";
            this.dt_fim.Size = new System.Drawing.Size(107, 22);
            this.dt_fim.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(161, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "até";
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(1414, 45);
            this.bt_ok.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(75, 30);
            this.bt_ok.TabIndex = 5;
            this.bt_ok.Text = "OK";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 547);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 13, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1618, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbStatus
            // 
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.Location = new System.Drawing.Point(16, 124);
            this.listView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1592, 403);
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "process.png");
            this.imageList1.Images.SetKeyName(1, "678134-sign-check-16.png");
            this.imageList1.Images.SetKeyName(2, "f-cross_256-16.png");
            // 
            // btnApagaOK
            // 
            this.btnApagaOK.Location = new System.Drawing.Point(1494, 44);
            this.btnApagaOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnApagaOK.Name = "btnApagaOK";
            this.btnApagaOK.Size = new System.Drawing.Size(112, 31);
            this.btnApagaOK.TabIndex = 6;
            this.btnApagaOK.Text = "Apagar OK";
            this.btnApagaOK.UseVisualStyleBackColor = true;
            this.btnApagaOK.Click += new System.EventHandler(this.btnApagaOK_Click);
            // 
            // txtPercNaoEntrega
            // 
            this.txtPercNaoEntrega.Location = new System.Drawing.Point(605, 46);
            this.txtPercNaoEntrega.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPercNaoEntrega.Name = "txtPercNaoEntrega";
            this.txtPercNaoEntrega.Size = new System.Drawing.Size(65, 22);
            this.txtPercNaoEntrega.TabIndex = 11;
            this.txtPercNaoEntrega.Text = "20";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(601, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "%Não";
            // 
            // cbClientes
            // 
            this.cbClientes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClientes.FormattingEnabled = true;
            this.cbClientes.Location = new System.Drawing.Point(311, 46);
            this.cbClientes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbClientes.Name = "cbClientes";
            this.cbClientes.Size = new System.Drawing.Size(285, 24);
            this.cbClientes.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(307, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Cliente";
            // 
            // txtThreads
            // 
            this.txtThreads.Location = new System.Drawing.Point(1341, 50);
            this.txtThreads.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtThreads.Name = "txtThreads";
            this.txtThreads.Size = new System.Drawing.Size(65, 22);
            this.txtThreads.TabIndex = 15;
            this.txtThreads.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1337, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Threads";
            // 
            // txtPercTemporarios
            // 
            this.txtPercTemporarios.Location = new System.Drawing.Point(764, 47);
            this.txtPercTemporarios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPercTemporarios.Name = "txtPercTemporarios";
            this.txtPercTemporarios.Size = new System.Drawing.Size(65, 22);
            this.txtPercTemporarios.TabIndex = 17;
            this.txtPercTemporarios.Text = "7";
            // 
            // txtQtdMinTemporarios
            // 
            this.txtQtdMinTemporarios.Location = new System.Drawing.Point(839, 47);
            this.txtQtdMinTemporarios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtQtdMinTemporarios.Name = "txtQtdMinTemporarios";
            this.txtQtdMinTemporarios.Size = new System.Drawing.Size(65, 22);
            this.txtQtdMinTemporarios.TabIndex = 18;
            this.txtQtdMinTemporarios.Text = "10";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(601, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "entrega";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(760, 27);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "temporário";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(760, 11);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "%Erro";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(835, 27);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "erro temp";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(835, 11);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Qtde min";
            // 
            // txtCampanhaId
            // 
            this.txtCampanhaId.Location = new System.Drawing.Point(133, 81);
            this.txtCampanhaId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCampanhaId.Name = "txtCampanhaId";
            this.txtCampanhaId.Size = new System.Drawing.Size(132, 22);
            this.txtCampanhaId.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 87);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "CampanhaId";
            // 
            // rbPeriodo
            // 
            this.rbPeriodo.AutoSize = true;
            this.rbPeriodo.Checked = true;
            this.rbPeriodo.Location = new System.Drawing.Point(15, 49);
            this.rbPeriodo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbPeriodo.Name = "rbPeriodo";
            this.rbPeriodo.Size = new System.Drawing.Size(17, 16);
            this.rbPeriodo.TabIndex = 26;
            this.rbPeriodo.TabStop = true;
            this.rbPeriodo.UseVisualStyleBackColor = true;
            // 
            // rbCampanhaId
            // 
            this.rbCampanhaId.AutoSize = true;
            this.rbCampanhaId.Location = new System.Drawing.Point(15, 87);
            this.rbCampanhaId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbCampanhaId.Name = "rbCampanhaId";
            this.rbCampanhaId.Size = new System.Drawing.Size(17, 16);
            this.rbCampanhaId.TabIndex = 27;
            this.rbCampanhaId.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(676, 26);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 17);
            this.label12.TabIndex = 30;
            this.label12.Text = "não entrega";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(677, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 17);
            this.label13.TabIndex = 29;
            this.label13.Text = "Qtde min";
            // 
            // txtQtdeMinNaoEntrega
            // 
            this.txtQtdeMinNaoEntrega.Location = new System.Drawing.Point(680, 46);
            this.txtQtdeMinNaoEntrega.Margin = new System.Windows.Forms.Padding(4);
            this.txtQtdeMinNaoEntrega.Name = "txtQtdeMinNaoEntrega";
            this.txtQtdeMinNaoEntrega.Size = new System.Drawing.Size(65, 22);
            this.txtQtdeMinNaoEntrega.TabIndex = 28;
            this.txtQtdeMinNaoEntrega.Text = "10";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(992, 30);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 17);
            this.label14.TabIndex = 36;
            this.label14.Text = "conf. ent.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(992, 14);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 17);
            this.label15.TabIndex = 35;
            this.label15.Text = "Qtde min";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(917, 30);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 17);
            this.label16.TabIndex = 34;
            this.label16.Text = "conf. ent.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(917, 14);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 17);
            this.label17.TabIndex = 33;
            this.label17.Text = "%Aguard.";
            // 
            // txtQtdeMinConfirmacaoEntrega
            // 
            this.txtQtdeMinConfirmacaoEntrega.Location = new System.Drawing.Point(996, 50);
            this.txtQtdeMinConfirmacaoEntrega.Margin = new System.Windows.Forms.Padding(4);
            this.txtQtdeMinConfirmacaoEntrega.Name = "txtQtdeMinConfirmacaoEntrega";
            this.txtQtdeMinConfirmacaoEntrega.Size = new System.Drawing.Size(65, 22);
            this.txtQtdeMinConfirmacaoEntrega.TabIndex = 32;
            this.txtQtdeMinConfirmacaoEntrega.Text = "100";
            // 
            // txtPercAguardConfirmacaoEntrega
            // 
            this.txtPercAguardConfirmacaoEntrega.Location = new System.Drawing.Point(921, 50);
            this.txtPercAguardConfirmacaoEntrega.Margin = new System.Windows.Forms.Padding(4);
            this.txtPercAguardConfirmacaoEntrega.Name = "txtPercAguardConfirmacaoEntrega";
            this.txtPercAguardConfirmacaoEntrega.Size = new System.Drawing.Size(65, 22);
            this.txtPercAguardConfirmacaoEntrega.TabIndex = 31;
            this.txtPercAguardConfirmacaoEntrega.Text = "7";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1618, 569);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtQtdeMinConfirmacaoEntrega);
            this.Controls.Add(this.txtPercAguardConfirmacaoEntrega);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtQtdeMinNaoEntrega);
            this.Controls.Add(this.rbCampanhaId);
            this.Controls.Add(this.rbPeriodo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtCampanhaId);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtQtdMinTemporarios);
            this.Controls.Add(this.txtPercTemporarios);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtThreads);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbClientes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPercNaoEntrega);
            this.Controls.Add(this.btnApagaOK);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dt_fim);
            this.Controls.Add(this.dt_ini);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmPrincipal";
            this.Text = "Verificação resumos SQL SERVER X Tables";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dt_ini;
        private System.Windows.Forms.DateTimePicker dt_fim;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripStatusLabel lbStatus;
        private System.Windows.Forms.Button btnApagaOK;
        private System.Windows.Forms.TextBox txtPercNaoEntrega;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbClientes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtThreads;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPercTemporarios;
        private System.Windows.Forms.TextBox txtQtdMinTemporarios;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCampanhaId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton rbPeriodo;
        private System.Windows.Forms.RadioButton rbCampanhaId;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtQtdeMinNaoEntrega;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtQtdeMinConfirmacaoEntrega;
        private System.Windows.Forms.TextBox txtPercAguardConfirmacaoEntrega;
    }
}

