﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Banco
{


    public class AcessoDados
    {
        private string _stringDeConexao;
        private const string usuario = "user_mc";
        //private const string usuario = "bytebr@s94svey6md"; //DESENV
        private const string senha = "Mc2012pp#";
        //private const string senha = "ec80c71q@#";

        public AcessoDados()
        {
            this._stringDeConexao = GetStringDeConexao();
        }

        private string GetStringDeConexao()
        {

            //string servidor = "xbvag1ik8u";
            //string banco = "messagecenter_br";
            string servidor = "mcserver5.messagecenter.com.br";
            string banco = "mcstorage";

            string strConexao = "";


            //if (servicoConfiguracao.ConsultaConfiguracaoTipoBanco() == "MSSQL")
            //{
            //    //var usuario = "";
            //    //var senha = "";
                strConexao = string.Format("Server = {0}; Database = {1}; User Id = {2}; Password = {3};", servidor, banco, usuario, senha);
            //}
            //else
            //{
            //    strConexao = string.Format("Server=tcp:{0}.database.windows.net;Database={1};User ID={2}@{0};Password={3};Trusted_Connection=False;Pooling=False;Encrypt=True;", servidor, banco, usuario, senha);
            //}

            return strConexao;
        }

        #region ExecuteDataTable

        public DataTable ExecuteDataTable(string strSQL, params Parametro[] parametros)
        {
            return this.ExecuteDataTable(strSQL, 60, parametros);
        }

        public DataTable ExecuteDataTable(string strSQL, int commandTimeout, params Parametro[] parametros)
        {
            var dt = new DataTable();
            using (var conexao = new SqlConnection(_stringDeConexao))
            {
                //conexao.Open();
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = commandTimeout;

                    //passa os parâmetros
                    foreach (var parametro in parametros)
                    {
                        var param = cmd.Parameters.AddWithValue(parametro.Nome, parametro.Valor);
                        param.IsNullable = true;

                        if (parametro.Valor == null)
                        {
                            param.Value = DBNull.Value;
                        }
                    }

                    using (var dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                        dr.Close();

                    }
                }
                conexao.Close();
            }
            return dt;
        }

        public DataTable ExecuteDataTable(string strConexao, string strSQL, params Parametro[] parametros)
        {
            return this.ExecuteDataTable(strConexao, 60, strSQL, parametros);
        }

        public DataTable ExecuteDataTable(string strConexao, int commandTimeout, string strSQL, params Parametro[] parametros)
        {
            var conexao = new SqlConnection(strConexao + string.Format(";User Id={0};Password={1};", usuario, senha));
            conexao.Open();

            var cmd = conexao.CreateCommand();
            cmd.CommandText = strSQL;
            cmd.CommandTimeout = commandTimeout;

            if (parametros != null)
            {
                //passa os parâmetros
                foreach (var parametro in parametros)
                {
                    var param = cmd.Parameters.AddWithValue(parametro.Nome, parametro.Valor);
                    param.IsNullable = true;

                    if (parametro.Valor == null)
                    {
                        param.Value = DBNull.Value;
                    }
                }
            }

            var dt = new DataTable();

            using (var dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
                dr.Close();
            }

            conexao.Close();

            return dt;
        }

        #endregion

        public void ExecuteNonQuery(string strSQL, params Parametro[] parametros)
        {
            this.ExecuteNonQuery(strSQL, 60, parametros);
        }

        public int ExecuteScalar(string strSQL, params Parametro[] parametros)
        {
            return this.ExecuteScalar(strSQL, 60, parametros);
        }

        public void ExecuteNonQuery(string strSQL, int commandTimeout, params Parametro[] parametros)
        {
            using (var conexao = new SqlConnection(_stringDeConexao))
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = commandTimeout;

                    //passa os parâmetros
                    foreach (var parametro in parametros)
                    {
                        var param = cmd.Parameters.AddWithValue(parametro.Nome, parametro.Valor);
                        param.IsNullable = true;
                        if (parametro.Valor == null)
                        {
                            param.Value = DBNull.Value;
                        }
                    }

                    cmd.ExecuteReader();
                }
                conexao.Close();
            }
        }

        public int ExecuteScalar(string strSQL, int commandTimeout, params Parametro[] parametros)
        {
            int id = 0;

            using (var conexao = new SqlConnection(_stringDeConexao))
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = commandTimeout;

                    //passa os parâmetros
                    foreach (var parametro in parametros)
                    {
                        var param = cmd.Parameters.AddWithValue(parametro.Nome, parametro.Valor);
                        param.IsNullable = true;

                        if (parametro.Valor == null)
                        {
                            param.Value = DBNull.Value;
                        }
                    }

                    id = Convert.ToInt32(cmd.ExecuteScalar());
                }
                conexao.Close();
            }

            return id;
        }

    }

}
