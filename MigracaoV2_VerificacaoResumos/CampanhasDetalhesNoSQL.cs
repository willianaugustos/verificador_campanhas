﻿using MC.Dominio.Entidades.NoSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigracaoV2_VerificacaoResumos
{
    public class CampanhasDetalhesNoSQL : EntidadeNoSqlBase
    {
        public string Item { get; set; }
        public string Informacao { get; set; }

        public string Grupo { get; set; }
        public int Status { get; set; }
    }
}
