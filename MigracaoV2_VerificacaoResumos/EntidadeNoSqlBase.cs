﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Dominio.Entidades.NoSql
{
    public class EntidadeNoSqlBase : TableEntity
    {
        public EntidadeNoSqlBase()
        {

        }

        public EntidadeNoSqlBase(string _PartitionKey, string _Rowkey)
        {
            this.PartitionKey = _PartitionKey;
            this.RowKey = _Rowkey;
        }
    }
}